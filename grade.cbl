       IDENTIFICATION DIVISION. 
       PROGRAM-ID. GRADE.
       AUTHOR. NARANYA BOONMEE.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT MY-GRADE-FILE ASSIGN TO  "mygrade.txt"
           ORGANIZATION IS LINE SEQUENTIAL.
           SELECT  WRITE-GRADE-FILE ASSIGN TO  "avg.txt"
           ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION.
       FD  MY-GRADE-FILE.
       01  GRADE-DETAILS.
           88 END-OF-MYGRADE-FILE  VALUE HIGH-VALUE .
           05 TOPIC-CODE           PIC X(6). 
           05 TOPIC-NAME           PIC X(50).
           05 TOPIC-CREDITS        PIC 9(1).
           05 TOPIC-GRADE          PIC X(2).
       
       FD  WRITE-GRADE-FILE.
       01  WRITE-DETAILS.
           88 END-OF-WRITEGRADE-FILE VALUE HIGH-VALUE .
           05 WRITE-GRADE          PIC X(15).
           05 WRITE-RESULT         PIC 9.999.

       WORKING-STORAGE SECTION. 
       01  AVG-GRADE               PIC 9(1)V999  VALUE ZERO .
       01  TOPIC                   PIC 99        VALUE ZERO.
       01  TOPIC-COUNT             PIC 9(2)      VALUE ZERO .
       01  GRADE-SUM               PIC 9(3)      VALUE ZERO .
       01  CREDIT-SUM              PIC 9(3)      VALUE ZERO .
       01  SUBJECT-GRADE           PIC 9(1)V9   VALUE ZERO .

       PROCEDURE DIVISION.
       BEGIN.
           OPEN INPUT  MY-GRADE-FILE
           PERFORM UNTIL END-OF-MYGRADE-FILE 
              READ MY-GRADE-FILE 
                 AT END SET END-OF-MYGRADE-FILE TO TRUE
              END-READ
           IF NOT END-OF-MYGRADE-FILE THEN
              PERFORM 001-CREDIT-SUM THRU 001-EXIT

           END-PERFORM
           CLOSE MY-GRADE-FILE
            

           OPEN  OUTPUT  WRITE-GRADE-FILE

           CLOSE  WRITE-GRADE-FILE
           .
        001-CREDIT-SUM.
           COMPUTE TOPIC-COUNT = TOPIC-COUNT + 1
           COMPUTE GRADE-SUM = GRADE-SUM + TOPIC-CREDITS 
           DISPLAY TOPIC-CODE SPACE TOPIC-NAME SPACE TOPIC-CREDITS
           SPACE TOPIC-GRADE 
           .
           
        001-CONVERT.
           IF TOPIC-GRADE IS EQUAL TO "A" THEN
              MOVE 4 TO SUBJECT-GRADE
           ELSE IF TOPIC-GRADE IS EQUAL TO "B+" THEN
              MOVE 3.5 TO SUBJECT-GRADE
           ELSE IF TOPIC-GRADE IS EQUAL TO "B"  THEN
              MOVE 3   TO SUBJECT-GRADE
           ELSE IF TOPIC-GRADE  IS EQUAL TO "C+" THEN
              MOVE 2.5 TO SUBJECT-GRADE 
           ELSE IF TOPIC-GRADE IS EQUAL TO "C" THEN
              MOVE 2 TO SUBJECT-GRADE
           ELSE IF TOPIC-GRADE IS EQUAL TO "D+" THEN
              MOVE 1.5 TO SUBJECT-GRADE
           ELSE IF TOPIC-GRADE IS EQUAL TO "D" THEN
              MOVE 1 TO SUBJECT-GRADE
           END-IF 
       .
       001-EXIT.
           EXIT
       .            